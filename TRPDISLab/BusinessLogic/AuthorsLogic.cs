﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class AuthorsLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _authorsAccessor;

        public AuthorsLogic(LibraryDataSet dataSet, AbstractAccessor countriesAccessor, AbstractAccessor authorsAccessor) : base(dataSet)
        {
            _countriesAccessor = countriesAccessor;
            _authorsAccessor = authorsAccessor;
        }

        public override void Read()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, DataSet);
                    _authorsAccessor.Read(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, DataSet);
                    _authorsAccessor.Update(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}

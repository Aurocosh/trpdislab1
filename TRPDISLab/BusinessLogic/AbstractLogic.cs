﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPDISLab.BusinessLogic
{
    public abstract class AbstractLogic
    {
        public LibraryDataSet DataSet { get; set; }

        protected AbstractLogic(LibraryDataSet dataSet)
        {
            DataSet = dataSet;
        }

        public abstract void Read();
        public abstract void Update();
    }
}

﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class PublishersLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publisherAccessor;

        public PublishersLogic(LibraryDataSet dataSet, AbstractAccessor countriesAccessor, AbstractAccessor publisherAccessor) : base(dataSet)
        {
            _countriesAccessor = countriesAccessor;
            _publisherAccessor = publisherAccessor;
        }

        public override void Read()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, DataSet);
                    _publisherAccessor.Read(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, DataSet);
                    _publisherAccessor.Update(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}

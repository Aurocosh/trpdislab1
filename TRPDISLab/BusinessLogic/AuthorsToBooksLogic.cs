﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class AuthorsToBooksLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publishersAccessor;
        private readonly AbstractAccessor _genresAccessor;
        private readonly AbstractAccessor _booksAccessor;
        private readonly AbstractAccessor _authorsAccessor;
        private readonly AbstractAccessor _authorToBookRelationAccessor;

        public AuthorsToBooksLogic(LibraryDataSet dataSet, AbstractAccessor countriesAccessor, AbstractAccessor publishersAccessor, AbstractAccessor genresAccessor, AbstractAccessor booksAccessor, AbstractAccessor authorsAccessor, AbstractAccessor authorToBookRelationAccessor) : base(dataSet)
        {
            _countriesAccessor = countriesAccessor;
            _publishersAccessor = publishersAccessor;
            _genresAccessor = genresAccessor;
            _booksAccessor = booksAccessor;
            _authorsAccessor = authorsAccessor;
            _authorToBookRelationAccessor = authorToBookRelationAccessor;
        }

        public override void Read()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, DataSet);
                    _publishersAccessor.Read(connection, transaction, DataSet);
                    _genresAccessor.Read(connection, transaction, DataSet);
                    _booksAccessor.Read(connection, transaction, DataSet);
                    _authorsAccessor.Read(connection, transaction, DataSet);
                    _authorToBookRelationAccessor.Read(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, DataSet);
                    _publishersAccessor.Update(connection, transaction, DataSet);
                    _genresAccessor.Update(connection, transaction, DataSet);
                    _booksAccessor.Update(connection, transaction, DataSet);
                    _authorsAccessor.Update(connection, transaction, DataSet);
                    _authorToBookRelationAccessor.Update(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}

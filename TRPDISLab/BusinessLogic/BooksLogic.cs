﻿using System;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLab.BusinessLogic
{
    public class BooksLogic : AbstractLogic
    {
        private readonly AbstractAccessor _countriesAccessor;
        private readonly AbstractAccessor _publisherAccessor;
        private readonly AbstractAccessor _genresAccessor;
        private readonly AbstractAccessor _booksAccessor;

        public BooksLogic(LibraryDataSet dataSet, AbstractAccessor countriesAccessor, AbstractAccessor publisherAccessor, AbstractAccessor genresAccessor, AbstractAccessor booksAccessor) : base(dataSet)
        {
            _countriesAccessor = countriesAccessor;
            _publisherAccessor = publisherAccessor;
            _genresAccessor = genresAccessor;
            _booksAccessor = booksAccessor;
        }

        public override void Read()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Read(connection, transaction, DataSet);
                    _publisherAccessor.Read(connection, transaction, DataSet);
                    _genresAccessor.Read(connection, transaction, DataSet);
                    _booksAccessor.Read(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Update()
        {
            using (var connection = ConnectionFactory.GetConnection())
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    _countriesAccessor.Update(connection, transaction, DataSet);
                    _publisherAccessor.Update(connection, transaction, DataSet);
                    _genresAccessor.Update(connection, transaction, DataSet);
                    _booksAccessor.Update(connection, transaction, DataSet);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}

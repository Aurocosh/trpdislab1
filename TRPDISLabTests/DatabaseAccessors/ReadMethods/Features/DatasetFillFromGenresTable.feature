﻿Feature: DatasetFillFromGenresTable
	I want to load data from pg database table genres with genresAccessor

@mytag
Scenario Outline: GenresDataSetFill
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded genres from "<dataString>"
	And i have prepared insert query for "genres" table with columns "genre_id,name"
	And i have added parameters "genre_id,name" with types "Integer,Varchar"
	And i have prepared data for insertion
	And i have executed insert query
	When i filled genres dataset from database
	And i have created datatable "dataTableExample" from genres
	And i loaded dataset table "genres" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

	Examples: 
| dataString                                                                                                                                                                                       |
|                                                                                                                                                                                                  |
| 1, 'Science fiction'                                                                                                                                                                             |
| 2, 'Satire';3, 'Drama'                                                                                                                                                                           |
| 4, 'Action and Adventure';5, 'Romance';6, 'Mystery'                                                                                                                                              |
| 7, 'Horror';8, 'Self help';9, 'Health';10, 'Guide';11, 'Travel';12, 'Children''s';13, 'Religion'                                                                                                 |
| 14, 'Spirituality & New Age';15, 'Science';16, 'History';17, 'Math';18, 'Anthology';19, 'Poetry'                                                                                                 |
| 20, 'Encyclopedias';21, 'Dictionaries';22, 'Comics';23, 'Art';24, 'Cookbooks';25, 'Diaries';26, 'Journals';27, 'Prayer books';28, 'Series';29, 'Trilogy';30, 'Biographies';31, 'Autobiographies' |

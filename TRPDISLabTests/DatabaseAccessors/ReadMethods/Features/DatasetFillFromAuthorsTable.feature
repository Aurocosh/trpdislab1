﻿Feature: DatasetFillFromAuthorsTable
	I want to load data from pg database table authors with authorsAccessor

@mytag
Scenario Outline: AuthorsDataSetFill
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded authors from "<dataString>"
	And i have determined authors dependecy on countries table
	And I have inserted dependecies in database table "countries", which have 2 columns "country_id,name" with "country_id" as key value. Default values "'country'"
	And i have prepared insert query for "authors" table with columns "author_id,name,surname,country_id"
	And i have added parameters "author_id,name,surname,country_id" with types "Integer,Varchar,Varchar,Integer"
	And i have prepared data for insertion
	And i have executed insert query
	When i filled authors dataset from database
	And i have created datatable "dataTableExample" from authors
	And i loaded dataset table "authors" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                                                                                                                                                                        |
|                                                                                                                                                                                                                                                                   |
| 1, 'Jane', 'Reid', 120                                                                                                                                                                                                                                            |
| 2, 'Brian', 'Hunt', 48; 3, 'Louise', 'Clark', 32                                                                                                                                                                                                                  |
| 4, 'Marie', 'Franklin', 91; 5, 'Lawrence', 'Fowler', 55; 6, 'Phillip', 'Bishop', 140                                                                                                                                                                              |
| 7, 'Carol', 'Cunningham', 83; 8, 'Lillian', 'Schmidt', 139; 9, 'Frank', 'Russell', 123; 10, 'Anna', 'Gray', 56; 11, 'Rose', 'Lewis', 148; 12, 'Jose', 'Reynolds', 60                                                                                              |
| 13, 'Christina', 'Gomez', 3; 14, 'Mildred', 'Carpenter', 136; 15, 'Lori', 'Simmons', 114; 16, 'Jesse', 'Rogers', 123; 17, 'Daniel', 'Gardner', 102; 18, 'Stephanie', 'Grant', 37; 19, 'Patrick', 'Weaver', 12; 20, 'Robert', 'Riley', 55; 21, 'Juan', 'Myers', 71 |
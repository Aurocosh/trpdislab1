﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests.DatabaseAccessors.ReadMethods.Steps
{
    [Binding]
    public class DataSetFillFromAuthorToBookRelationTableSteps
    {
        [When(@"i have created datatable ""(.*)"" from author to book relations")]
        public void WhenIHaveCreatedDatatableFromAuthorToBookRelations(string dataTableName)
        {
            var containerList = (List<TestObjectContainer>)ScenarioContext.Current["containerList"];
            var authorToBookRelations = containerList.Cast<AuthorToBookRelation>().ToList();
            ScenarioContext.Current[dataTableName] = AuthorToBookRelation.ListToDataTable(authorToBookRelations);
        }

        [Given(@"i filled author to book relations dataset from database")]
        [When(@"i filled author to book relations dataset from database")]
        public void WhenIFilledAuthorToBookRelationsDatasetFromDatabase()
        {
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataset = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var authorToBookRelationAccessor = new AuthorToBookRelationAccessor();
            var transaction = сonnection.BeginTransaction();
            authorToBookRelationAccessor.Read(сonnection, transaction, dataset);
            transaction.Commit();
        }
    }
}

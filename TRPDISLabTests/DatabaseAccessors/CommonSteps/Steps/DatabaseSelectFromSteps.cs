﻿using System;
using System.Data;
using System.Data.SqlClient;
using Npgsql;
using TechTalk.SpecFlow;
using TRPDISLab;

namespace TRPDISLabTests.DatabaseAccessors.CommonSteps.Steps
{
    [Binding]
    public class DatabaseSelectFromSteps
    {
        [Given(@"i loaded database table ""(.*)"" into ""(.*)"" datatable")]
        [When(@"i loaded database table ""(.*)"" into ""(.*)"" datatable")]
        public void WhenILoadedDatabaseTableIntoDatatable(string tableName, string dataTableName)
        {
            var command = $"SELECT * FROM {tableName} ORDER BY 1";
            var connection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataTable = new DataTable();
            using (var adapter = new NpgsqlDataAdapter(command, connection.Connection))
            {
                adapter.Fill(dataTable);
            }
            ScenarioContext.Current[dataTableName] = dataTable;
        }
    }
}

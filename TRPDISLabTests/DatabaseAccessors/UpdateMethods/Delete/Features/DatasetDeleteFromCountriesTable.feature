﻿Feature: DatasetDeleteFromCountriesTable
	I want to load data from pg database table countries with countriesAccessor delete some rows and update original database

@mytag
Scenario Outline: CountriesDataSetDelete
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded countries from "<dataString>"
	And i have prepared insert query for "countries" table with columns "country_id,name"
	And i have added parameters "country_id,name" with types "Integer,Varchar"
	And i have prepared data for insertion
	And i have executed insert query
	And i filled countries dataset from database
	And i have deleted several "<rows>" from dataset table "countries"
	When i have called update method of countriesAccessor
	And i loaded dataset table "countries" into "dataTableExample" datatable
	And i loaded database table "countries" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

	Examples: 
| dataString                                                                                                         | rows  |
|                                                                                                                    |       |
| 1, China                                                                                                           | 0     |
| 2, Colombia; 3, China                                                                                              | 0     |
| 4, Bangladesh; 5, North Korea; 6, Russia                                                                           | 1     |
| 7, Thailand; 8, China; 9, Brazil; 10, China; 11, Philippines; 12, Afghanistan                                      | 1,3   |
| 13, Ukraine; 14, Ireland; 15, China; 16, Ukraine; 17, Czech Republic; 18, Cuba; 19, China; 20, China; 21, Portugal | 3,4,5 |


﻿Feature: DatasetInsertIntoCountriesTable
	I want to insert data into pg database table countries with dataset and countriesAccessor

@mytag
Scenario Outline: CountriesDataSetInsert
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded countries from "<dataString>"
	And i have prepared data for insertion
	And i have inserted data into dataset table "countries"
	When i have called update method of countriesAccessor
	And i loaded dataset table "countries" into "dataTableExample" datatable
	And i loaded database table "countries" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                         |
|                                                                                                                    |
| 1, China                                                                                                           |
| 2, Colombia; 3, China                                                                                              |
| 4, Bangladesh; 5, North Korea; 6, Russia                                                                           |
| 7, Thailand; 8, China; 9, Brazil; 10, China; 11, Philippines; 12, Afghanistan                                      |
| 13, Ukraine; 14, Ireland; 15, China; 16, Ukraine; 17, Czech Republic; 18, Cuba; 19, China; 20, China; 21, Portugal |
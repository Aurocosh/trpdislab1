﻿Feature: DatasetInsertIntoAuthorToBookRelationTable
	I want to insert data into pg database table author_to_book_relation with dataset and authorToBookRelationAccessor

@mytag
Scenario Outline: AuthorsDataSetInsert
	Given I provided valid database connection
	And i prepared test database
	And i created new dataset
	And i loaded author to book relations from "<dataString>"

	And i have inserted in "genres" table row with columns "genre_id,name" and values "1,'genre'"
	And i have inserted in "countries" table row with columns "country_id,name" and values "1,'country'"
	And i have inserted in "publishers" table row with columns "publisher_id,name,country_id" and values "1,'publisher',1"

	And i have determined author to book relations dependecy on book table
	And I have inserted dependecies in database table "books", which have 5 columns "book_id,name,genre_id,publication_date,publisher_id" with "book_id" as key value. Default values "'Do Androids Dream of Electric Sheep?', 1, '1942-10-27', 1"
	And i have determined author to book relations dependecy on authors table
	And I have inserted dependecies in database table "authors", which have 4 columns "author_id,name,surname,country_id" with "author_id" as key value. Default values "'Jane', 'Reid', 1"
	
	And i have prepared data for insertion
	And i have inserted data into dataset table "author_to_book_relation"
	When i have called update method of authorToBookRelationAccessor
	And i loaded dataset table "author_to_book_relation" into "dataTableExample" datatable
	And i loaded database table "author_to_book_relation" into "dataTableResult" datatable
	Then example datatable should be equal to result datatable
	And connection should be closed

Examples: 
| dataString                                                                                                |
|                                                                                                           |
| 1, 132, 67                                                                                                |
| 2, 33, 59;3, 550, 26                                                                                      |
| 4, 523, 85;5, 573, 40;6, 341, 87                                                                          |
| 7, 600, 85;8, 262, 5;9, 143, 69;10, 635, 15;11, 14, 36;12, 231, 53                                        |
| 13, 42, 51;14, 388, 41;15, 172, 52;16, 203, 45;17, 96, 33;18, 492, 65;19, 397, 46;20, 406, 42;21, 205, 94 |
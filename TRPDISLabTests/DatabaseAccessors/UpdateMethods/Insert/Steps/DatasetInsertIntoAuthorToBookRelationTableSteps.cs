﻿using System;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLabTests.DatabaseAccessors.UpdateMethods.Insert.Steps
{
    [Binding]
    public class DatasetInsertIntoAuthorToBookRelationTableSteps
    {
        [When(@"i have called update method of authorToBookRelationAccessor")]
        public void WhenIHaveCalledUpdateMethodOfAuthorToBookRelationAccessor()
        {
            var authorsAccessor = new AuthorToBookRelationAccessor();
            var сonnection = (AbstractConnection)ScenarioContext.Current["connection"];
            var dataSet = (LibraryDataSet)ScenarioContext.Current["dataset"];

            var transaction = сonnection.BeginTransaction();
            authorsAccessor.Update(сonnection, transaction, dataSet);
            transaction.Commit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPDISLabTests.Containers
{
    public abstract class TestObjectContainer
    {
        public abstract object[] GetAsObjectArray();
    }
}

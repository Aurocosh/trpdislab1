﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using TRPDISLabTests.Containers;

namespace TRPDISLabTests
{
    public class Book : TestObjectContainer
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public int GenreId { get; set; }
        public DateTime PublicationDate { get; set; }
        public int PublisherId { get; set; }

        public Book(int BookId, string Name, int GenreId, DateTime PublicationDate, int PublisherId)
        {
            this.BookId = BookId;
            this.Name = Name;
            this.GenreId = GenreId;
            this.PublicationDate = PublicationDate;
            this.PublisherId = PublisherId;
        }

        public static List<TestObjectContainer> DeserializeList(string list)
        {
            if (list.Trim().Length == 0) return new List<TestObjectContainer>();

            var books = new List<Book>();
            var booksStringList = list.Split(';').ToList();
            foreach (var bookDataString in booksStringList)
            {
                var bookData = bookDataString.Split(',').Select(v => v.Trim()).ToList();
                var id = int.Parse(bookData[0]);
                var name = bookData[1];
                var genreId = int.Parse(bookData[2]);
                var publicationDate = DateTime.ParseExact(bookData[3], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var publisherId = int.Parse(bookData[4]);

                var book = new Book(id, name, genreId, publicationDate, publisherId);
                books.Add(book);
            }

            return books.Cast<TestObjectContainer>().ToList(); ;
        }

        public static DataTable ListToDataTable(List<Book> books)
        {
            // Here we create a DataTable with four columns.
            var tableBooks = new DataTable();
            tableBooks.Columns.Add("book_id", typeof(int));
            tableBooks.Columns.Add("name", typeof(string));
            tableBooks.Columns.Add("genre_id", typeof(int));
            tableBooks.Columns.Add("publication_date", typeof(DateTime));
            tableBooks.Columns.Add("publisher_id", typeof(int));

            foreach (var book in books)
            {
                tableBooks.Rows.Add(book.BookId, book.Name, book.GenreId
                    , book.PublicationDate, book.PublisherId);
            }

            return tableBooks;
        }

        public override object[] GetAsObjectArray()
        {
            object[] data = { BookId, Name, GenreId, PublicationDate, PublisherId };
            return data;
        }
    }
}

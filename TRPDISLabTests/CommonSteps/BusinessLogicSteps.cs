﻿using System;
using Spring.Context.Support;
using TechTalk.SpecFlow;
using TRPDISLab;
using TRPDISLab.BusinessLogic;
using TRPDISLab.DataBaseAccessors;

namespace TRPDISLabTests.CommonSteps
{
    [Binding]
    public class BusinessLogicSteps
    {
        [When(@"i have used ""(.*)"" busines logic method read on ""(.*)"" dataset")]
        [Given(@"i have used ""(.*)"" busines logic method read on ""(.*)"" dataset")]
        public void GivenIHaveUsedBusinesLogicMethodReadOnDataset(string logicType, string dataSetName)
        {
            var ctx = ContextRegistry.GetContext();
            var logicClassName = TestHelper.MapToLogicClassName(logicType);
            var logic = (AbstractLogic)ctx.GetObject(logicClassName);
            logic.DataSet = ScenarioContext.Current.Get<LibraryDataSet>(dataSetName);
            logic.Read();
        }

        [When(@"i have used ""(.*)"" busines logic method update on ""(.*)"" dataset")]
        [Given(@"i have used ""(.*)"" busines logic method update on ""(.*)"" dataset")]
        public void GivenIHaveUsedBusinesLogicMethodUpdateOnDataset(string logicType, string dataSetName)
        {
            var ctx = ContextRegistry.GetContext();
            var logicClassName = TestHelper.MapToLogicClassName(logicType);
            var logic = (AbstractLogic)ctx.GetObject(logicClassName);
            logic.DataSet = ScenarioContext.Current.Get<LibraryDataSet>(dataSetName);
            logic.Update();
        }
    }
}

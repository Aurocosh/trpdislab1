insert into publishers (publisher_id, name, country_id) values (1, 'Pearson', 56);
insert into publishers (publisher_id, name, country_id) values (2, 'ThomsonReuters', 133);
insert into publishers (publisher_id, name, country_id) values (3, 'RELX Group', 70);
insert into publishers (publisher_id, name, country_id) values (4, 'Wolters Kluwer', 69);
insert into publishers (publisher_id, name, country_id) values (5, 'Penguin Random House', 38);
insert into publishers (publisher_id, name, country_id) values (6, 'Phoenix Publishing and Media Company', 148);
insert into publishers (publisher_id, name, country_id) values (7, 'China South Publishing Group', 118);
insert into publishers (publisher_id, name, country_id) values (8, 'Hachette Livre', 109);
insert into publishers (publisher_id, name, country_id) values (9, 'McGraw-Hill Education', 92);
insert into publishers (publisher_id, name, country_id) values (10, 'Holtzbrinck', 83);
insert into publishers (publisher_id, name, country_id) values (11, 'Grupo Planeta', 29);
insert into publishers (publisher_id, name, country_id) values (12, 'Scholastic', 138);
insert into publishers (publisher_id, name, country_id) values (13, 'Wiley', 81);
insert into publishers (publisher_id, name, country_id) values (14, 'Cengage Learning Holdings II LP', 124);
insert into publishers (publisher_id, name, country_id) values (15, 'China Publishing Group Corporation', 52);
insert into publishers (publisher_id, name, country_id) values (16, 'Harper Collins', 114);
insert into publishers (publisher_id, name, country_id) values (17, 'Houghton Mifflin Harcourt', 116);
insert into publishers (publisher_id, name, country_id) values (18, 'De Agostini Editore', 80);
insert into publishers (publisher_id, name, country_id) values (19, 'Oxford University Press', 14);
insert into publishers (publisher_id, name, country_id) values (20, 'Springer Science and Business Media', 64);
insert into publishers (publisher_id, name, country_id) values (21, 'China Education Publishing Holdings', 35);
insert into publishers (publisher_id, name, country_id) values (22, 'Informa', 111);
insert into publishers (publisher_id, name, country_id) values (23, 'Shueisha', 31);
insert into publishers (publisher_id, name, country_id) values (24, 'Kodansha Ltd.', 94);
insert into publishers (publisher_id, name, country_id) values (25, 'Egmont Group', 138);
insert into publishers (publisher_id, name, country_id) values (26, 'Shogakukan', 15);
insert into publishers (publisher_id, name, country_id) values (27, 'Bonnier', 123);
insert into publishers (publisher_id, name, country_id) values (28, 'Grupo Santillana', 146);
insert into publishers (publisher_id, name, country_id) values (29, 'Kadokawa Publishing', 56);
insert into publishers (publisher_id, name, country_id) values (30, 'Simon & Schuster', 31);
insert into publishers (publisher_id, name, country_id) values (31, 'Woongjin ThinkBig', 55);
insert into publishers (publisher_id, name, country_id) values (32, 'Klett', 127);
insert into publishers (publisher_id, name, country_id) values (33, 'Groupe Madrigall', 92);
insert into publishers (publisher_id, name, country_id) values (34, 'Les Editions Lefebvre-Sarrut', 141);
insert into publishers (publisher_id, name, country_id) values (35, 'Messagerie GeMS', 135);
insert into publishers (publisher_id, name, country_id) values (36, 'Media Participations', 80);
insert into publishers (publisher_id, name, country_id) values (37, 'Mondadori', 133);
insert into publishers (publisher_id, name, country_id) values (38, 'Cambridge University Press', 14);
insert into publishers (publisher_id, name, country_id) values (39, 'The Perseus Book Group', 68);
insert into publishers (publisher_id, name, country_id) values (40, 'France Loisirs', 71);
insert into publishers (publisher_id, name, country_id) values (41, 'Westermann Verlagsgruppe', 117);
insert into publishers (publisher_id, name, country_id) values (42, 'Sanoma', 70);
insert into publishers (publisher_id, name, country_id) values (43, 'Cornelsen', 146);
insert into publishers (publisher_id, name, country_id) values (44, 'Kyowon Co. Ltd.', 3);
insert into publishers (publisher_id, name, country_id) values (45, 'La Martinière', 40);
insert into publishers (publisher_id, name, country_id) values (46, 'Haufe Gruppe', 99);
insert into publishers (publisher_id, name, country_id) values (47, 'WEKA', 22);
insert into publishers (publisher_id, name, country_id) values (48, 'RCS Libri', 44);
insert into publishers (publisher_id, name, country_id) values (49, 'Gakken Co. Ltd.', 61);
insert into publishers (publisher_id, name, country_id) values (50, 'Bungeishunju Ltd.', 5);
insert into publishers (publisher_id, name, country_id) values (51, 'OLMA Media Group', 57);
insert into publishers (publisher_id, name, country_id) values (52, 'EKSMO', 31);
insert into publishers (publisher_id, name, country_id) values (53, 'Groupe Albin Michel', 148);
insert into publishers (publisher_id, name, country_id) values (54, 'Saraiva', 104);
insert into publishers (publisher_id, name, country_id) values (55, 'Editora FTD', 118);
insert into publishers (publisher_id, name, country_id) values (56, 'Abril Edu', 85);
insert into publishers (publisher_id, name, country_id) values (57, 'Shinchosha Publishing Co', 77);

﻿-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.2
-- Project Site: pgmodeler.com.br
-- Model Author: ---

--SET check_function_bodies = false;
-- ddl-end --

--SET search_path TO pg_catalog,public,library;
-- ddl-end --

-- object: books | type: TABLE --
CREATE TABLE books(
	book_id integer NOT NULL,
	name varchar(128) NOT NULL,
	genre_id integer NOT NULL,
	publication_date date,
	publisher_id integer,
	CONSTRAINT primary_key_book PRIMARY KEY (book_id)

);
-- ddl-end --
ALTER TABLE books OWNER TO tester;
-- ddl-end --

-- object: authors | type: TABLE --
CREATE TABLE authors(
	author_id integer NOT NULL,
	name varchar(128) NOT NULL,
	surname varchar(128) NOT NULL,
	country_id integer,
	CONSTRAINT primary_key_author PRIMARY KEY (author_id)

);
-- ddl-end --
ALTER TABLE authors OWNER TO tester;
-- ddl-end --

-- object: countries | type: TABLE --
CREATE TABLE countries(
	country_id integer NOT NULL,
	name varchar(128) NOT NULL,
	CONSTRAINT primary_key_country PRIMARY KEY (country_id)

);
-- ddl-end --
ALTER TABLE countries OWNER TO tester;
-- ddl-end --

-- object: publishers | type: TABLE --
CREATE TABLE publishers(
	publisher_id integer NOT NULL,
	name varchar(128) NOT NULL,
	country_id integer,
	CONSTRAINT primary_key_publisher PRIMARY KEY (publisher_id)

);
-- ddl-end --
ALTER TABLE publishers OWNER TO tester;
-- ddl-end --

-- object: genres | type: TABLE --
CREATE TABLE genres(
	genre_id integer NOT NULL,
	name varchar NOT NULL,
	CONSTRAINT primary_key_genre PRIMARY KEY (genre_id)

);
-- ddl-end --
ALTER TABLE genres OWNER TO tester;
-- ddl-end --

-- object: author_to_book_relation | type: TABLE --
CREATE TABLE author_to_book_relation(
	atbr_id integer NOT NULL,
	author_id integer NOT NULL,
	book_id integer NOT NULL,
	CONSTRAINT primary_key_atbr PRIMARY KEY (atbr_id)

);
-- ddl-end --
ALTER TABLE author_to_book_relation OWNER TO tester;
-- ddl-end --

-- object: foreign_book_genre | type: CONSTRAINT --
ALTER TABLE books ADD CONSTRAINT foreign_book_genre FOREIGN KEY (genre_id)
REFERENCES genres (genre_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: foreign_book_publisher | type: CONSTRAINT --
ALTER TABLE books ADD CONSTRAINT foreign_book_publisher FOREIGN KEY (publisher_id)
REFERENCES publishers (publisher_id) MATCH SIMPLE
ON DELETE RESTRICT ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: foreign_author_country | type: CONSTRAINT --
ALTER TABLE authors ADD CONSTRAINT foreign_author_country FOREIGN KEY (country_id)
REFERENCES countries (country_id) MATCH SIMPLE
ON DELETE SET NULL ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: foreign_publisher_country | type: CONSTRAINT --
ALTER TABLE publishers ADD CONSTRAINT foreign_publisher_country FOREIGN KEY (country_id)
REFERENCES countries (country_id) MATCH SIMPLE
ON DELETE SET NULL ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: foreign_atbr_author | type: CONSTRAINT --
ALTER TABLE author_to_book_relation ADD CONSTRAINT foreign_atbr_author FOREIGN KEY (author_id)
REFERENCES authors (author_id) MATCH SIMPLE
ON DELETE CASCADE ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: foreign_atbr_book | type: CONSTRAINT --
ALTER TABLE author_to_book_relation ADD CONSTRAINT foreign_atbr_book FOREIGN KEY (book_id)
REFERENCES books (book_id) MATCH FULL
ON DELETE CASCADE ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --



